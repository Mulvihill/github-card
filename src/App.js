import React, {Component} from 'react';
import './App.css';
import {Card, Image} from 'semantic-ui-react'

const GIT_URL = accountURL => `https://api.github.com/users/${accountURL}`;

class App extends Component {
  state = {
    user: {},
    active: false
  }

  handleToggle = () => {
    if (this.state.active === true) {this.setState({active: false})}
    else {this.setState({active: true})};
    fetch(GIT_URL('cedricmul')).then(res => res.json()).then(data =>{
      this.setState({user: data})
    })
  }
  
  render (){
    return (
    <div>
      <button onClick={this.handleToggle}>Toggle</button>
      {this.state.active === true && (
      <Card>  
          <Image src={this.state.user.avatar_url} alt="User Avatar"/>
          <Card.Content>
            <Card.Header>{this.state.user.login}</Card.Header>
            <Card.Description>{this.state.user.bio}</Card.Description>
          </Card.Content>
      </Card>
        )}
    </div>
  )};
}

export default App;
